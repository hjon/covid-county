package covid.county

import covid.county.util.BadgeJson
import covid.county.util.CsvDownload
import java.io.File
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Locale

class NYT {

    fun getData() {
        val sourceLink = "https://github.com/nytimes/covid-19-data/raw/master/us-counties.csv"
        val csvFile = CsvDownload().downloadCSVFile(sourceLink)

        getDataByCounty(csvFile, "Ramsey", "Minnesota", 550000, true)

        //neighboring counties
        getDataByCounty(csvFile, "Anoka", "Minnesota", 357000, false)
        getDataByCounty(csvFile, "Dakota", "Minnesota", 429000, false)
        getDataByCounty(csvFile, "Hennepin", "Minnesota", 1266000, false)
        getDataByCounty(csvFile, "Washington", "Minnesota", 262000, false)

        // cleanup
        println("Deleting ${csvFile.path}")
        csvFile.delete()
    }

    private fun getDataByCounty(csvFile: File, county:String, state: String, population: Int, writeBadge: Boolean) {
        // val csvFile = temporaryReadFromFile()
        val countyLines = filterCountyData(csvFile, county, state)
        println("Found ${countyLines.size} days")

        // get the date and daily count as a map
        val dateCounts = mutableMapOf<String, Int>()
        var previousValue = 0
        countyLines.forEach { nextLine ->
                val parts = nextLine.split(",")
                val dateString = parts.get(0)
                val total = parts.get(4).toInt()
                val dailyCount = total - previousValue
                previousValue = total
                dateCounts.put(dateString, dailyCount)
        }

        var index = 0
        val caseCount = mutableListOf<Int>()
        val csvLines = mutableListOf<String>("date,cases,daily")
        dateCounts.forEach { key, value ->
            caseCount.add(value)
            if (index > 13) {
                // val date = stringToDate(key, "MMMM d yyyy")
                var lastTwoWeeks = 0
                for (i in (index - 14)..index) {
                    lastTwoWeeks += caseCount[i]
                }
                val casesPer10k = lastTwoWeeks.toFloat() / (population / 10000)
                csvLines.add("$key,$casesPer10k,$value")
            }
            index++
        }

        //Write out the CSV file
        val outputFile = File("build/nyt-${county.toLowerCase()}.csv")
        println("Writing results to ${outputFile.path}")
        outputFile.writeText(csvLines.joinToString("\n"))

        //Save details for GitLab badge
        if(writeBadge) {
            val lastRecordParts = csvLines.last().split(",")
            BadgeJson().outputJsonForBadge("build/badge-nyt.json", lastRecordParts[0], lastRecordParts[1])
        }
    }

    private fun temporaryReadFromFile(): File {
        val fileName = "/Users/nohr/Downloads/us-counties.csv"
        return File(fileName)
    }

    private fun filterCountyData(csvFile: File, county: String, state: String): List<String> {
        println("Filtering to just $county data...")
        val outputLines = mutableListOf<String>()
        val countyLookup = "$county,$state"
        csvFile.forEachLine() { nextLine ->
            if (nextLine.indexOf(countyLookup) > 0) {
                outputLines.add(nextLine)
            }
         }

         // println("Found ${outputLines.size} lines")
         return outputLines
    }

    private fun stringToDate(input: String, pattern: String): LocalDate {
        val formatter = DateTimeFormatter.ofPattern(pattern, Locale.ENGLISH)
        return LocalDate.parse(input, formatter)
    }
}
