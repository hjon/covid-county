package covid.county.util

import java.io.File

/**
 * This output JSON just to be used by GitLab badge
 */
class BadgeJson {
    fun outputJsonForBadge(fileName: String, lastDate: String, lastRate: String) {
        //only need 2 decimal places for the rate
        val lastRateFloat = lastRate.toFloat()
        val lastRateFormatted = "%.2f".format(lastRateFloat)

        //build the JSON
        val b = StringBuilder()
        b.append("{")
        b.append("\"date\": ")
        b.append("\"${lastDate}\"")
        b.append(", ")
        b.append("\"rate\": ")
        b.append("\"${lastRateFormatted}\"")
        b.append("}")

        println("Writing badge info to ${fileName} : ${b.toString()}")
        
        //write the file
        File(fileName).writeText(b.toString())
    }
}